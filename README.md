# Peertube to Discord

Automatically send new PeerTube videos to Discord

## How to use

1. Create a `config.json` file:

    ```json
    {
        "instance": {
            "url": "https://peertube.example.com",
            "name": "PeerTube instance"
        },
        "cache": {
            "file": "/path/to/cache.txt",
            "size": 50
        },
        "webhook_url": "https://discord.com/api/webhooks/server_id/webhook_secret",
        "embed_color": 16742929,
        "cron_mode": false,
        "fetch_delay": 300
    }
    ```

    You can copy it from `config.example.json`.

    - `instance.url` - instance base URL
    - `instance.name` - name of the instance visible in embeds
    - `cache.file` - location of a cache file used to keep track of new videos
    
    Must be read-writable to the process.

    - `cache.size` - number of video IDs that will be kept in the cache
    
    It needs to be at least 10 for the program to work properly, but it should be more than that (50 is a safe value for most, if not all, instances).

    - `webhook_url` - URL to send POST requests to
    
    Copy that from Discord.

    - `embed_color` - color value for the embed decoration
    - `cron_mode` - specifies if the program quits or blocks after finishing

        - `true` - cron mode - quit after finishing operation

        Use if you want to run the script periodically, for example by using cron.

        - `false` - daemon mode - sleep and repeat after finishing

        Use if you want to run this as a daemon.
    
    - `delay` - time (in seconds) to sleep for in daemon mode

    Can be ommited when using cron mode.

2. Run:

    ```sh
    ./peertube_discord config.json
    ```

    You probably want to run this as a cron job or as a daemon.
